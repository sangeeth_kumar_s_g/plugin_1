angular.module('starter.controllers', [])

//MENU//
 
.controller('AppCtrl', function() {})

//CAMERA//
.controller('CameraCtrl', function($ionicPlatform, $rootScope, $scope, $cordovaCamera) {
    $ionicPlatform.ready(function() {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true
        };
 
        $scope.takePicture = function() {
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.imgSrc = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });
        }
 
    });
})

//SOCIAL//

.controller('ShareCtrl', function($ionicPlatform, $scope, $cordovaSocialSharing) {
    $ionicPlatform.ready(function() {
 
        var message = 'Join the Treasure Hunt powered by Orion Orbit. Click on orionorbit.com/signup?ref=sangeeth';
        var subject = 'Join Treasure Hunt powered by Orion Orbit';
        var link = 'orionorbit.com/signup?ref=sangeeth';
 
        $scope.nativeShare = function() {
            $cordovaSocialSharing
                .share(message, subject, link); // Share via native share sheet
        };
 
    });
});
